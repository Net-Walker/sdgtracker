
@extends('layouts.app_no_footer')

@section('content')

<div class="wrapper">
<!-- Sidebar -->
<nav id="sidebar">
    <div class="row">
      <div class="col-lg-12">
      <div class="mobile-sidebar-close">    
          <img width="15" id="sidebarCollapse2" src="{{asset('public/img/close.png')}}">
        </div>
        <ul class="navbar-nav goal-menu mb-3">
          <li class="nav-item dropdown">
            <a class="nav-link" style="font-size:1rem!important; padding-top: 0;" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span><img class="select-icon" src="https://res.cloudinary.com/hy20xjlga/image/upload/v1547824869/favicon.png"></span> Select a Goal <i class="fa fa-chevron-down dropdown-icon"></i>
            </a>
            <div class="dropdown-menu " data-mcs-theme="minimal" aria-labelledby="navbarDropdown">
            @foreach($goals as $i => $goal)
              <a class="dropdown-item" href="{{route('indicators-details',['parameter'=>$goal->goal_id])}}"><img class="menu-icon" src="{{$goal->icon}}">Goal {{$i+1}}: {{str_limit($goal->title, $limit = 25, $end = '...')}}</a>
            @endforeach
            </div>
          </li>
        </ul>
        <div class="side-goal-menu">
          <div>
          <img src="{{$goal_->icon}}">
          </div>
          <h2>Goal {{$goal_->goal_id}} {{$goal_->title}}</h2>
              <p>{{$targets->count()}} Targets, <span>{{$count_indicators}} Indicators</span></p>
        </div>
      </div>
    </div>
</nav>
<!-- Page Content -->
<div id="content">
    <div class="mobile-sidebar-toggle">    
      <img width="20" id="sidebarCollapse" src="{{asset('public/img/menu-button.png')}}">
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="tracker-details">
          <div class="row">
            <div class="col-10">
              <h2 class="mb-2">{{$goal_->long_title}}</h2>
            </div>
            <div class="col-2" align="right">
              <span><a href="{{URL::previous()}}" class="btn btn-light btn-sm view-btn" tabindex="-1" role="button" aria-disabled="true"><img class="bck-icon" src="{{asset('public/img/chevron-left.svg')}}">Go Back</a></span>
            </div>
          </div>
          @foreach($indicators as $n => $target)
          <div class="row">
            <div class="col-lg-12">
              <div class="tracker-card">
                <h3 class="target-desc" style="background-color:{{$goal_->background_color}}">{{$target->target_title}}</h3>
                <div class="row">
                  <div class="col-lg-12">
                  @foreach($target->indicators as $i =>  $indicator)
                    <h4 class="indic-title">SDG Indicator <span>{{$goal_->goal_id}}.{{$n+1}}.{{$i+1}}</span></h4>
                    <p class="indic-desc">{{$indicator->title}}</p>
                    <div class="row mt-4">
                      <div class="col-lg-6">
                        <div id="map">
                        @if(!$indicator->indicator_trackers->isEmpty())
                        <div id="chart_div{{$indicator->id}}"></div> 
                        @else
                        <h3 class="empty-state-msg">We currently do not have data for this indicator. You can notify us of available data for this indicator via public opinion.</h3>
                        @endif
                        </div>
                        <ul class="dash-nav nav nav-pills mb-3" id="pills-tab" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active" role="tab" aria-controls="pills-home" aria-selected="true">Line Graph</a>
                          </li>
                        </ul>
                      </div>
                      <div class="col-lg-6">
                        <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade" id="pills-profile{{$indicator->id}}" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div id="chart">
                            @if(!$indicator->indicator_trackers->isEmpty())
                            <div id="regions_div{{$indicator->id}}"></div>
                            @else
                              <h3 class="empty-state-msg">We currently do not have data for this indicator. You can notify us of available data for this indicator via public opinion.</h3>
                            @endif
                            </div>
                          </div>
                          <div class="tab-pane fade show active" id="pills-home{{$indicator->id}}" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div id="chart">
                            @if(!$indicator->indicator_trackers->isEmpty())
                            <div id="map_div{{$indicator->id}}" style="height: 550px;"></div>
                            @else
                              <h3 class="empty-state-msg">We currently do not have data for this indicator. You can notify us of available data for this indicator via public opinion.</h3>
                            @endif
                            </div>
                          </div>
                        </div>
                        <ul class="dash-nav nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                              <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile{{$indicator->id}}" role="tab" aria-controls="pills-profile" aria-selected="false">Bar Chart</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home{{$indicator->id}}" role="tab" aria-controls="pills-home" aria-selected="true">Map</a>
                            </li>
                        </ul>

                      </div>
                    </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach

        </div>
      </div>
    </div>
</div>
</div> 
  @foreach($indicators as $target)
    @foreach($target->indicators as $indicator)
    <script type="text/javascript">
      google.charts.load('current', {
          'packages':['geochart'],
          // Note: you will need to get a mapsApiKey for your project.
          // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
          'mapsApiKey': 'AIzaSyD_AEe6GL5H_CtlsngNkYVpJSsnyPCXlLY'
      });
      google.charts.setOnLoadCallback(drawRegionsMap);
      function drawRegionsMap() {
          var data = google.visualization.arrayToDataTable([
            ['State', 'Percentage'],
            @foreach ($indicator->indicator_trackers as $tracker)
              ["{{$tracker->states->region}}", {{$tracker->progress}}], 
            @endforeach
          ]);
          var options = {
          // chart: {
          //   title: 'Box Office Earnings in First Two Weeks of Opening',
          //   subtitle: 'in millions of dollars (USD)'
          // },
            width: 500, height:500 , region: "NG", resolution: "provinces",
            title: '{{$indicator->map_title}}',
          };
          var chart = new google.visualization.GeoChart(document.getElementById('map_div{{$indicator->id}}'));
          chart.draw(data, options);
      }
    </script>
    
      @endforeach
  @endforeach

@foreach($indicators as  $target)
  @foreach($target->indicators as  $indicator)
  <script type="text/javascript">
    google.charts.load('current', {packages: ['corechart', 'line']});
    google.charts.setOnLoadCallback(drawAxisTickColors);
    function drawAxisTickColors() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Day');
        data.addColumn('number', 'Progress');
        data.addRows([
            @foreach ($indicator->indicator_trackers as $tracker)
                ["{{$tracker->year}}", {{$tracker->progress}}], 
            @endforeach
        ]);
        var options = {
          width: 500, height:500 ,
        legend: { position: 'bottom', alignment: 'start' },
        curveType: 'function',
        vAxis:{
          gridlineColor: 'EEEEEE',
          format:  '#\'%\'',        
        },
        hAxis:{
          gridlineColor: '#fff',
        },
        title: '{{$indicator->graph_title}}',
          colors: ['#a52714', '#e5243b']
        };
        var chart = new google.visualization.LineChart(document.getElementById('chart_div{{$indicator->id}}'));
        chart.draw(data, options);
        }
  </script>
  
  @endforeach
@endforeach

@foreach($indicators as  $target)
  @foreach($target->indicators as  $indicator)
  <script type="text/javascript">
    google.charts.load('current', {packages: ['corechart', 'line']});
    google.charts.setOnLoadCallback(drawAxisTickColors);
    function drawAxisTickColors() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Day');
        data.addColumn('number', 'Progress');
        data.addRows([
            @foreach ($indicator->indicator_trackers as $tracker)
                ["{{$tracker->year}}", {{$tracker->progress}}], 
            @endforeach
        ]);
        var options = {
          width: 500, height:500 ,
        legend: { position: 'bottom', alignment: 'start' },
        curveType: 'function',
        vAxis:{
          gridlineColor: 'EEEEEE',
          format:  '#\'%\'',        
        },
        hAxis:{
          gridlineColor: '#fff',
        },
        title: '{{$indicator->graph_title}}',
            colors: ['#a52714', '#e5243b']
        };
        var chart = new google.visualization.ColumnChart(document.getElementById('regions_div{{$indicator->id}}'));
        chart.draw(data, options);
      }
  </script>
  @endforeach
@endforeach

@endsection
