@extends('layouts.app')
@section('content')
  <section id="about-page-bn">
      <h1 class="about-header">About The Tracker</h1>
  </section>
      
  <section id="about-page">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-10 offset-1">
          <h2>SDG Tracker for Nigeria.</h2>
          <p>
            Voices around the world are demanding for leadership role and direction in the fight against poverty, inequality, and climate change. To turn these demands into actions, world leaders gathered on 25 September, 2015, at the United Nations in New York to adopt the 2030 Agenda for Sustainable Development.
          </p>
          <p>
            The 2030 Agenda comprises 17 new Sustainable Development Goals (SDGs), or Global Goals, which will guide policy and funding for the next 15 years, beginning with a historic pledge to end poverty. Everywhere. Permanently.
          </p>
          <p>
            The Global Goals replace the Millennium Development Goals (MDGs), which in September 2000 rallied the world around a common 15-year agenda to tackle the indignity of poverty.
          </p>
          <p>
            This new development agenda applies to all countries, promotes peaceful and inclusive societies, creates better jobs and tackles the environmental challenges of our time—particularly climate change.

            The Global Goals must finish the job that the MDGs started, and leave no one behind.
          </p>
          <p>
            Our SDG Tracker presents data across all available indicators from our database, using official statistics from the UN and other international organizations. It is a free, open-access publication that tracks progress towards the SDGs in Nigeria and allows people the citizens of Nigeria to hold the governments accountable to achieving the agreed goals.
          </p>
          <p>
            The 17 Sustainable Development Goals are defined in a list of 169 SDG Targets. Progress towards these Targets is agreed to be tracked by 232 unique Indicators. Here is the full list of definitions.
          </p>
          <p>
            We will keep this up-to-date with the most recent data and SDG developments through to the end of the 2030 Agenda.
          </p>
          <p>
            For many Indicators data is available, but major data gaps remain. If you are aware of high-quality data we have yet to include please notify us. We hope that this collaborative approach allows us to support the United Nations in developing the most complete and up-to-date source for tracking progress to 2030.
          </p>
        </div>
        <!-- <div class="col-lg-6">
          <img width="600" src="img/sdg-poster.png">
        </div> -->
      </div>
    </div>
  </section>

  <section id="partners">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-4">
          <h2 class="">Our Partners</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
        <div class="col-md-8" align="right">
          <img src="{{asset('public/img/partners/1.png')}}">
          <img src="{{asset('public/img/partners/5.jpg')}}">
        </div>
      </div>
    </div>
  </section>
@endsection