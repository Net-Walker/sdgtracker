@extends('layouts.admin')

@section('content')

    <div class="content-wrapper">
        <div class="head">
            <p>Indicators</p>
            <!-- <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">
            Add Indicatos
            </button> -->
        </div>
        <div class="row" style="margin-top:20px;">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title" style="text-align:left">All Indicators</h4>
                        <table class="table">
                            <thead>
                                <tr>
                                <th>Indicator</th>
                                <th>Action</th>
                                <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($indicators as $indicator)
                                    <tr>
                                        <td style="width:75%">
                                            <a href="{{route('indicator_',['parameter'=>$indicator->id])}}" class="link">{{$indicator->title}}</a>
                                        </td>
                                        <td>
                                            <label class="badge badge-danger" style="background-color:#00AED9;border:none;"><a href="#" data-toggle="modal" data-target="#editindicator{{$indicator->id}}" class="edit_delete">Edit</a> | <a href="{{route('delete_indicator',$indicator->id)}}" class="edit_delete" onclick="return confirm('Are you sure?')">Delete</a></label>
                                        </td>
                                        <td >
                                        <a href="{{route('manage_tracker',['parameter'=>$indicator->id])}}" class="link">Manage</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Modal -->
    @foreach($indicators as $indicator)
    <div class="modal fade" id="editindicator{{$indicator->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabeledit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Indicator</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="forms-sample" method="POST" action="{{ route('edit_indicator') }}">
                        @csrf
                        <div class="form-group">
                        <input type="text" class="form-control"  hidden name="indicator_id" value="{{$indicator->id}}" required autofocus>
                        <label for="exampleInputEmail1">Indicator</label>
                            <textarea class="form-control" id="exampleTextarea1" name="indicator"rows="6" required autofocus>{{$indicator->title}} </textarea>
                        </div>
                        <button type="submit" class="btn btn-primary mr-2">Update</button>
                        <button class="btn btn-light"  class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    <!-- End Modal -->
@endsection
