@extends('layouts.admin')

@section('content')

<div class="content-wrapper">
    <div class="head">
        <p>Manage Indicator</p>
        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">
        Input Data
        </button>
    </div>

    <div class="row" style="margin-top:20px;">
        <div class="col-md-6">
            <div id="chart_div" style=" height: 500px;"></div>
        </div>
        <div class="col-md-6">
            <div id="regions_div" style="height: 400px;"></div>
        </div>
    </div>

    <div class="row" style="margin-top:20px;">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title" style="text-align:left"> {{$indicator->title}} </h4>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th> States </th>
                                <th> Tracker </th>
                                <th> Date </th>
                                <th> Manage </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($trackers as $tracker)
                            @csrf
                            <tr>
                            <td>
                                {{$tracker->states->name}}
                            </td>
                            <td>
                            <div class="progress-wrapper">
                            <div class="progress-info">
                                <div class="progress-label">
                                <span></span>
                                </div>
                                <div class="progress-percentage">
                                <span>{{$tracker->progress}}%</span>
                                </div>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-primary goal17" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{$tracker->progress}}%;"></div>
                            </div>
                            </div>
                            </td>
                            <td>
                                {{$tracker->year}}
                            </td>
                            <td>
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#editindicator{{$tracker->id}}">Update</button>
                                <a href="{{route('delete_indicator_progress',['parameter'=>$tracker->id])}}" class="btn btn-sm btn-danger" style="background:red" onclick="return confirm('Are you sure?')">Delete</a>
                            </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Record New Progress</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Record data for percentage(%) {{$indicator->title}}</p>
                <form class="forms-sample" method="POST" action="{{ route('record_progress') }}">
                    @csrf
                    <div class="form-group">
                        <input class="form-control" placeholder="10" type="hidden" value="{{$indicator->id}}" name="indicator_id"/>
                        <label for="exampleInputUsername1"> State</label>
                        <select class="form-control" name="ng_state" required>
                            @foreach($states as $state)
                                <option value="{{$state->id}}">{{$state->name}}</option>
                            @endforeach
                        </select>
                        <p style="font-size:10px;padding:2px;color:red">Where data applies to nigeria as a whole select nigeria from the dropdown</p>
                    </div>

                    <div class="form-group">
                    <label for="exampleInputEmail1">Growth (In percentage %)</label>
                    <input class="form-control" placeholder="10" required type="text" name="growth"/>
                    </div>
                    <div class="form-group">
                    <label for="exampleInputEmail1">Date</label>
                        <input class="form-control" placeholder="YYYY" required type="text" name="year"/>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal -->

<!-- Edit Modal -->
@foreach($trackers as $tracker)
<div class="modal fade" id="editindicator{{$tracker->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabeledit" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Tracker</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form class="forms-sample" method="POST" action="{{ route('update_progress') }}">
                    @csrf
                    <div class="form-group">
                        <input class="form-control" placeholder="10" type="hidden" value="{{$tracker->id}}" name="tracker_id"/>
                        <label for="exampleInputUsername1"> State</label>
                        <select class="form-control" name="ng_state" required>
                            @foreach($states as $state)
                                <option value="{{$state->id}}" {{($state->id == $tracker->states->id) ? 'selected' : '' }}> {{$state->name}} </option>
                            @endforeach
                        </select>
                        <p style="font-size:9px;padding:2px">Where data applies to nigeria as a whole select nigeria from the dropdown</p>
                    </div>

                    <div class="form-group">
                    <label for="exampleInputEmail1">Growth (In percentage %)</label>
                    <input class="form-control" placeholder="10" required type="text" name="growth" value="{{$tracker->progress}}"/>
                    </div>
                    <div class="form-group">
                    <label for="exampleInputEmail1">Date</label>
                        <input class="form-control" placeholder="YYY" required type="text" name="year" value="{{$tracker->year}}"/>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach
<!-- End Modal -->

@foreach($trackers as $tracker)
<script>
function updateTextInput{{$tracker->id}}(val) {
    document.getElementById('rangeIndicator{{$tracker->id}}').innerHTML = val+'%'; 
}
</script>
@endforeach
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {
        'packages':['geochart'],
        // Note: you will need to get a mapsApiKey for your project.
        // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
        'mapsApiKey': 'AIzaSyD_AEe6GL5H_CtlsngNkYVpJSsnyPCXlLY'
    });
    google.charts.setOnLoadCallback(drawRegionsMap);
  
    function drawRegionsMap() {
        var data = google.visualization.arrayToDataTable([
          ['State', 'Percentage'],
          @foreach ($trackers as $tracker)
            ["{{$tracker->states->region}}", {{$tracker->progress}}], 
          @endforeach

        ]);

        var options = {
        // chart: {
        //   title: 'Box Office Earnings in First Two Weeks of Opening',
        //   subtitle: 'in millions of dollars (USD)'
        // },
          width: 500, height:500 , region: "NG", resolution: "provinces"
        };

        var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

        chart.draw(data, options);
    }
</script>

<script type="text/javascript">
    google.charts.load('current', {packages: ['corechart', 'line']});
    google.charts.setOnLoadCallback(drawAxisTickColors);

    function drawAxisTickColors() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Day');
          data.addColumn('number', 'Progress');

        data.addRows([
            @foreach ($trackers_graphs as $trackers_graph)
                ["{{$trackers_graph->year}}", {{$trackers_graph->progress}}], 
            @endforeach
        ]);
        var options = {
        legend: { position: 'bottom', alignment: 'start' },
        curveType: 'function',
        vAxis:{
            gridlineColor: 'EEEEEE',
            format:  '#\'%\'',        
        },
        hAxis:{
            gridlineColor: '#fff',
        },
            colors: ['#a52714', '#e5243b']
        };
        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
        }
</script>

@endsection
