@extends('layouts.admin')
@section('content')
<div class="content-wrapper">
    <div class="head">
        <p>User's Managements</p>
        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">
        Add User
        </button>
    </div>
    <div class="row mt5" style="margin-top:20px;">
    <div class="col-md-12">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title" style="text-align:left">All Users</h4>
                        <p class="card-description" style="text-align:left">
                        
                        </p>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Role</th>
                                    <th>Email</th>
                                    <th>Actions</th>
                               </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td class="py-1">
                                            <p class="link"> <img src="{{$user->avatar}}" style="border-radius:0px;" alt="image"/></p>
                                        </td>

                                        <td>
                                            <p>{{$user->name}}</p>
                                        </td>
                                        <td>
                                           <p>{{$user->role}}</p>
                                        </td>
                                        <td>
                                            <p>{{$user->email}}</p>
                                        </td>
                                        <td>
                                        <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
                                            <div class="dropdown-menu">
                                                <button class="dropdown-item" data-toggle="modal" data-target="" class="edit_delete">Suspend</button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New Administrator</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="forms-sample" method="POST" action="{{ route('create_user') }}">
                @csrf
                    <div class="form-group">
                        <label for="exampleInputUsername1">Full Name</label>
                        <input type="text" class="form-control" id="exampleInputUsername1" placeholder="Erhun Abbe" name="name" value="{{ old('name') }}" required autofocus>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="developer@gmail.com" name="email" value="{{ old('email') }}" required>
                    </div>
                    <div class="form-group">
                      <label>Job Role</label>
                        <select class="form-control" name="role">
                          <option value="Administrator">Administrator</option>
                          <option value="Manager">Manager</option>
                          <option value="Editor">Editor</option>
                        </select>
                      </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="*******"  name="password" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputConfirmPassword1">Confirm Password</label>
                        <input type="password" class="form-control" id="exampleInputConfirmPassword1" placeholder="*******" name="password_confirmation" required>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal -->
@endsection
