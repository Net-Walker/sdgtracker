@extends('layouts.admin')

@section('content')

<div class="content-wrapper">
{{$target->target_title}}
    <div class="row">
        <div class="col-md-12 mt-5">
        <div class="card">
                <div class="card-body">
                <h4 class="card-title" style="text-align:left">All Targets</h4>
                <table class="table">
                        <thead>
                            <tr>
                            <th>Indicator</th>
                            <th>Progress</th>
                            <th>Action</th>
                            <th>Manage</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($indicators as $indicator)
                                <tr>
                                    <td style="width:75%">
                                        <a href="{{route('indicator_',['parameter'=>$indicator->id])}}" class="link">{{$indicator->title}}</a>
                                    </td>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: {{$indicator->progress}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </td>
                                    <td>
                                        <label class="badge badge-danger" style="background-color:#00AED9;border:none;"><a href="#" data-toggle="modal" data-target="#editindicator{{$indicator->id}}" class="edit_delete">Edit</a> | <a href="{{route('delete_indicator',$indicator->id)}}" class="edit_delete" onclick="return confirm('Are you sure?')">Delete</a></label>
                                    </td>
                                    <td >
                                        <a href="{{route('manage_tracker',['parameter'=>$indicator->id])}}" class="link">Manage</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
