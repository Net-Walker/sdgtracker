@extends('layouts.admin')

@section('content')

<div class="content-wrapper">
{{$goal->goal_id}}: {{$goal->long_title}}
    <div class="row">
        <div class="col-md-12 mt-5">
            <div class="card">
                <div class="card-body">
                <h4 class="card-title" style="text-align:left">All Targets</h4>
                <table class="table">
                        <thead>
                            <tr>
                            <th>Title</th>
                            <th>Indicator's</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($targets as $target)
                                <tr>
                                    <td style="width:75%">
                                        <a href="{{route('target_',['parameter'=>$target->id])}}" class="link">{{$target->target_title}}</a>
                                    </td>
                                    <td>
                                        {{$target->indicators->count()}} <a href="{{route('indicators_',['parameter'=>$target->id])}}">Indicators</a>
                                    </td>
                                    <td>
                                        <label class="badge badge-danger" style="background-color:{{$goal->background_color}};border:none;"><a href="#" data-toggle="modal" data-target="#edittarget{{$target->id}}" class="edit_delete">Edit</a> | <a href="{{route('delete_target',$target->id)}}" class="edit_delete" onclick="return confirm('Are you sure?')">Delete</a></label>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
