@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <div class="head">
        <p>Public Opinions</p>
    </div>
    <div class="row" style="margin-top:20px;">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <!-- <h4 class="card-title" style="text-align:left">All d</h4> -->
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Do you think Nigeria will achieve its goals by 2030.</th>
                            <th>Which area do you think need the most attention in Nigeria?</th>
                            <th>User's Comments</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($opinions as $opinion)
                                <tr>
                                    <td>
                                        <p>{{$opinion->achieve_goals}}</p>
                                    </td>
                                    <td>
                                        <p>{{$opinion->goals_that_needs_attention}}</p>
                                    </td>
                                    <td style="width:35%">
                                        <p>{{$opinion->feedback}}</p>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
