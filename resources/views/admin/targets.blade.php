@extends('layouts.admin')

@section('content')

<div class="content-wrapper">
    <div class="head">
        <p>Targets</p>
        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">
        Add Target
        </button>
    </div>
    <div class="row" style="margin-top:20px;">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title" style="text-align:left">{{$goal_->long_title}}</h4>
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Title</th>
                            <th>Indicator's</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($targets as $target)
                                <tr>
                                    <td style="width:75%">
                                        <a href="{{route('target_',['parameter'=>$target->id])}}" class="link">{{$target->target_title}}</a>
                                    </td>
                                    <td>
                                        {{$target->indicators->count()}} <a href="{{route('indicators_',['parameter'=>$target->id])}}">Indicators</a>
                                    </td>
                                    <td>
                                        <label class="badge badge-danger" style="background-color:{{$goal_->background_color}};border:none;"><a href="#" data-toggle="modal" data-target="#edittarget{{$target->id}}" class="edit_delete">Edit</a> | <a href="{{route('delete_target',$target->id)}}" class="edit_delete" onclick="return confirm('Are you sure?')">Delete</a></label>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New Target</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="forms-sample" method="POST" action="{{ route('create_target') }}">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputUsername1"> Goal</label>
                        <input type="text" class="form-control"  disabled name="" value="{{$goal_->long_title}}" required autofocus>
                        <input type="text" class="form-control"  hidden name="goal_id" value="{{$goal_->id}}" required autofocus>
                    </div>

                    <div class="form-group">
                    <label for="exampleInputEmail1">Target</label>
                        <textarea class="form-control" id="exampleTextarea1" name="target"rows="6" required autofocus></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal -->

<!-- Edit Modal -->
@foreach($targets as $target)
<div class="modal fade" id="edittarget{{$target->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabeledit" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Target</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="forms-sample" method="POST" action="{{ route('edit_target') }}">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputUsername1"> Goal</label>
                        <input type="text" class="form-control"  disabled name="" value="{{$goal_->long_title}}" required autofocus>
                        <input type="text" class="form-control"  hidden name="target_id" value="{{$target->id}}" required autofocus>
                    </div>

                    <div class="form-group">
                    <label for="exampleInputEmail1">Target</label>
                        <textarea class="form-control" id="exampleTextarea1" name="target"rows="6" required autofocus>{{$target->target_title}} </textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                    <button class="btn btn-light" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach
<!-- End Modal -->

@endsection
