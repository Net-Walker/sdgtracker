@extends('layouts.admin')

@section('content')

<div class="content-wrapper">
<div class="head">
        <p>SDGs</p>
        <!-- <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">
        Add Goal
        </button> -->
    </div>

    <div class="row" style="margin-top:20px;">
        <div class="col-md-12">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title" style="text-align:left">All Goals</h4>
                        <p class="card-description" style="text-align:left">
                        
                        </p>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Goals</th>
                                    <th>Title</th>
                                    <!-- <th>Progress</th> -->
                                    <th>Targets</th>
                                    <th>Interventions</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($goals as $goal)
                                    <tr>
                                        <td class="py-1">
                                            <a href="{{route('goal_',['parameter'=>$goal->id])}}" class="link"> <img src="{{$goal->icon}}" style="border-radius:0px;" alt="image"/></a>
                                        </td>
                                        <td style="width:60%">
                                            <a href="{{route('goal_',['parameter'=>$goal->id])}}" class="link">{{$goal->long_title}}</a>
                                        </td>
                                        <!-- <td>
                                            <div class="progress">
                                                <div class="progress-bar bg-success" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </td> -->
                                        <td>
                                            {{$goal->targets->count()}} <a href="{{route('targets_',['parameter'=>$goal->goal_id])}}">Targets</a>
                                        </td>
                                        <td>
                                            {{$goal->interventions->count()}} <a href="{{route('interventions_',['parameter'=>$goal->goal_id])}}">Interventions</a>
                                        </td>
                                        <td>
                                        <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
                                            <div class="dropdown-menu">
                                                <button class="dropdown-item" data-toggle="modal" data-target="#edittarget{{$goal->id}}" class="edit_delete">Edit</button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
    </div>
</div>

<!-- Edit Modal -->
@foreach($goals as $goal)
<div class="modal fade" id="edittarget{{$goal->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabeledit" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Goal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="forms-sample" method="POST" action="{{ route('edit_goal') }}">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputUsername1"> Goal Title</label>
                        <input type="text" class="form-control"  hidden name="goal_id" value="{{$goal->id}}" required autofocus>
                        <input type="text" class="form-control"  name="title" value="{{$goal->title}}" required autofocus>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputUsername1"> Goal Long Title</label>
                        <input type="text" class="form-control"   name="long_title" value="{{$goal->long_title}}" required autofocus>
                    </div>

                    <div class="form-group">
                    <label for="exampleInputEmail1">Description</label>
                        <textarea class="form-control" id="exampleTextarea1" name="description"rows="6" required autofocus>{{$goal->description}} </textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                    <button class="btn btn-light" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach
<!-- End Modal -->
@endsection
