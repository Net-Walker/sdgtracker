@extends('layouts.admin')

@section('content')

<div class="content-wrapper">
    <div class="head">
        <p>Interventions</p>
        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">
        Add Intervention
        </button>
    </div>
    <div class="row" style="margin-top:20px;">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title" style="text-align:left">{{$goal_->long_title}}</h4>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($interventions as $intervention)
                                <tr>
                                    <td style="width:90%">
                                        <a href="{{route('intervention_',['parameter'=>$intervention->id])}}" class="link">{{$intervention->intervention_title}}</a>
                                    </td>
                                    <td>
                                        <label class="badge badge-danger" style="background-color:{{$goal_->background_color}};border:none;"><a href="#" data-toggle="modal" data-target="#editintervention{{$intervention->id}}" class="edit_delete">Edit</a> | <a href="{{route('delete_intervention',$intervention->id)}}" class="edit_delete" onclick="return confirm('Are you sure?')">Delete</a></label>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New Intervention</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="forms-sample" method="POST" action="{{ route('create_intervention') }}">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputUsername1"> Goal</label>
                        <input type="text" class="form-control"  disabled name="" value="{{$goal_->long_title}}" required autofocus>
                        <input type="text" class="form-control"  hidden name="goal_id" value="{{$goal_->id}}" required autofocus>
                    </div>

                    <div class="form-group">
                    <label for="exampleInputEmail1">Intervention</label>
                        <textarea class="form-control" id="exampleTextarea1" name="intervention" rows="6" required autofocus></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal -->

<!-- Edit Modal -->
@foreach($interventions as $intervention)
<div class="modal fade" id="editintervention{{$intervention->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabeledit" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Intervention</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="forms-sample" method="POST" action="{{ route('edit_intervention') }}">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputUsername1"> Goal</label>
                        <input type="text" class="form-control"  disabled name="" value="{{$goal_->long_title}}" required autofocus>
                        <input type="text" class="form-control"  hidden name="intervention_id" value="{{$intervention->id}}" required autofocus>
                    </div>

                    <div class="form-group">
                    <label for="exampleInputEmail1">Intervention</label>
                        <textarea class="form-control" id="exampleTextarea1" name="intervention"rows="6" required autofocus>{{$intervention->intervention_title}} </textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                    <button class="btn btn-light" type="button" class="close" data-dismiss="modal" aria-label="Close">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach
<!-- End Modal -->

@endsection