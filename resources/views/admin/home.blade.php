@extends('layouts.admin')

@section('content')

<div class="content-wrapper">
    Home
    <div class="row">
        <div class="col-md-8 mt-5">
            <div class="goal-wrap">
                @foreach($goals as $goal)
                <a href="{{route('goal_',['parameter'=>$goal->id])}}"><img class="img-responsive" src="{{$goal->icon}}"></a>
                @endforeach
            </div>
        </div>
        <div class="col-md-4">
            <div class="comment-section">

            </div>
        </div>
    </div>
</div>
@endsection
