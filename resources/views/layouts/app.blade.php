<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'SDG Tracker For Nigeria - Measuring progress towards the Sustainable Development Goals') }}</title>
        <link rel="stylesheet" href="{{ asset('public/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('public/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('public/css/hover-min.css') }}">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,700,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Marck+Script" rel="stylesheet">
        <link rel="icon" type="image/ico" href="{{ asset('public/favicon.ico') }}" />
        @toastr_css
    </head>
    <body>
        <nav class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light">
            <div class="container-fluid">
            <a class="navbar-brand" href="{{ route('index') }}"><img src="{{asset('public/img/oosap.png')}}"></a>
            <button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#navbar-primary" aria-controls="navbar-primary" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar-primary">
                <div class="navbar-collapse-header">
                <div class="row">
                    <div class="col-6 collapse-brand">
                    <a href="{{ route('index') }}">
                        <img src="{{asset('public/img/logo.png')}}">
                    </a>
                    </div>
                    <div class="col-6 collapse-close">
                    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-primary" aria-controls="navbar-primary" aria-expanded="false" aria-label="Toggle navigation">
                        <span></span>
                        <span></span>
                    </button>
                    </div>
                </div>
                </div>
                <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
                <!-- <li class="nav-item">
                    <a class="nav-link" href="{{ route('index') }}">SDG's
                    <span class="sr-only">(current)</span>
                    </a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('indicators',['parameter'=>1]) }}">SDG Tracker</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('indicators-details',['parameter'=>1])}}">Indicators</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" data-toggle="modal" data-target="#modal-default">Public Opinion</a>
                </li>
                </ul>
                <ul class="navbar-nav align-items-lg-center ml-lg-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('about') }}">About SDG Tracker</a>
                </li>
                </ul>
            </div>
            </div>
        </nav>
        @yield('content')
        <footer>
            <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 no-gutter">
                <p>© <?php echo date("Y");?>. Office of the Special Assistant to the President on SDGs.</p>
                </div>
                <div class="col-lg-6 no-gutter">
                <ul>
                    <li><a href="{{ route('indicators',['parameter'=>1])}}">SDG Tracker</a></li>
                    <!-- <li><a href="{{ route('contact-us') }}">Contact Us</a></li> -->
                    <li><a href="{{ route('login') }}">Login</a></li>
                </ul>
                </div>
            </div>
            </div>
        </footer>
        <div class="modal fade poll-modal" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
            <form  method="POST" action="{{ route('record_public_opinions') }} ">
                @csrf
                <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h6 class="modal-title" id="modal-title-default"></h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        </div>
                        <div class="modal-body">
                        <div id="p-opinion">
                        <h2 class="mt-3">What do you think?</h2>
                        <p>
                        The Sustainable Development Goals (SDGs) are a
                        universal call to action to end poverty, protect the planet
                        and ensure that all people enjoy peace and prosperity.
                        </p>
                        <h3>Do you think Nigeria will achieve its goals by 2030.</h3>
                            
                                <div class="custom-control custom-radio mb-3 mr-3">
                                    <input  class="custom-control-input" id="customRadio1" value="Yes" type="radio" name="achieve_goals">
                                    <label class="custom-control-label" for="customRadio1">
                                    <span>Yes</span>
                                    </label>
                                </div>
                                <div class="custom-control custom-radio mb-3 mr-3">
                                    <input  class="custom-control-input" id="customRadio2" value="No" type="radio" name="achieve_goals">
                                    <label class="custom-control-label" for="customRadio2">
                                    <span>No</span>
                                    </label>
                                </div>
                                <div class="custom-control custom-radio mb-3 mr-3">
                                    <input  class="custom-control-input" id="customRadio3" value="Don't Know" type="radio" name="achieve_goals">
                                    <label class="custom-control-label" for="customRadio3">
                                    <span>Don't know</span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                                <h3>Which area do you think need the most attention in Nigeria?</h3> 

                                <div class="poll-goal-wrap">
                                @foreach($goals as $goal)
                                    <input type="radio" class="radio_item" value="{{$goal->long_title}}" id="radio{{$goal->goal_id}}" name="goals_that_needs_attention">
                                    <label class="label_item" for="radio{{$goal->goal_id}}" > <img src="{{$goal->icon}}"> </label>
                                @endforeach
                                </div>
                                <div class="form-group">
                                    <h3>Your Comments</h3>
                                    <textarea class="form-control" id="exampleTextarea1" rows="4" required autofocus name="feedback"></textarea>
                                </div>
                            
                        </div>
                        </div>
                        <div class="modal-footer pb-5">
                        <button type="submit" class="btn btn-primary poll-btn">Save changes</button>
                        <button class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <script src="{{ asset('public/js/jquery.min.js') }}"></script>
        <script src="{{ asset('public/js/popper.min.js') }}"></script>
        <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('public/js/argon.js') }}"></script>
        <script src="{{ asset('public/js/headroom.min.js') }}"></script>
        <script src="{{ asset('admin/js/sweetalert.min.js') }}"></script>
        <!-- Include this after the sweet alert js file -->
        @include('sweet::alert')
        @jquery
        @toastr_js
        @toastr_render
    </body>
</html>

