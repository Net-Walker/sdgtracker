<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'SDG Tracker For Nigeria - Measuring progress towards the Sustainable Development Goals') }}</title>
        <link rel="stylesheet" href="{{ asset('public/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('public/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('public/css/hover-min.css') }}">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,700,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Marck+Script" rel="stylesheet">
        <link rel="icon" type="image/ico" href="{{ asset('public/favicon.ico') }}" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light">
            <div class="container-fluid">
            <a class="navbar-brand" href="{{ route('index') }}"><img src="{{asset('public/img/oosap.png')}}"></a>
            <button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#navbar-primary" aria-controls="navbar-primary" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar-primary">
                <div class="navbar-collapse-header">
                <div class="row">
                    <div class="col-6 collapse-brand">
                    <a href="{{ route('index') }}">
                        <img src="{{asset('public/img/logo.png')}}">
                    </a>
                    </div>
                    <div class="col-6 collapse-close">
                    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-primary" aria-controls="navbar-primary" aria-expanded="false" aria-label="Toggle navigation">
                        <span></span>
                        <span></span>
                    </button>
                    </div>
                </div>
                </div>
                <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
                <!-- <li class="nav-item">
                    <a class="nav-link" href="{{ route('index') }}">SDG's
                    <span class="sr-only">(current)</span>
                    </a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('indicators',['parameter'=>1]) }}">SDG Tracker</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('indicators-details',['parameter'=>1])}}">Indicators</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" data-toggle="modal" data-target="#modal-default">Public Opinion</a>
                </li>
                </ul>
                <ul class="navbar-nav align-items-lg-center ml-lg-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('about') }}">About Tracker</a>
                </li>
                </ul>
            </div>
            </div>
        </nav>
        @yield('content')
        <div class="modal fade poll-modal" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
            <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default"></h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                </div>
                <div class="modal-body">
                <div id="p-opinion">
                <h2 class="mt-3">SDG Tracker for Nigeria By The Office of the<br/> Special Assistant to the President on SDGs.</h2>
                <p>
                  The Sustainable Development Goals (SDGs) are a <br/>
                  universal call to action to end poverty, protect the planet <br/>
                  and ensure that all people enjoy peace and prosperity.
                </p>
                <p class="action hvr-icon-wobble-horizontal">Select a Goal on the right to get started.</p>
                    <form class="mb-3">
                    <div class="custom-control custom-radio mb-3 mr-3">
                        <input name="custom-radio-1" class="custom-control-input" id="customRadio1" type="radio">
                        <label class="custom-control-label" for="customRadio1">
                        <span>Yes</span>
                        </label>
                    </div>
                    <div class="custom-control custom-radio mb-3 mr-3">
                        <input name="custom-radio-1" class="custom-control-input" id="customRadio2" type="radio">
                        <label class="custom-control-label" for="customRadio2">
                        <span>No</span>
                        </label>
                    </div>
                    <div class="custom-control custom-radio mb-3 mr-3">
                        <input name="custom-radio-1" class="custom-control-input" id="customRadio3" type="radio">
                        <label class="custom-control-label" for="customRadio3">
                        <span>Don't know</span>
                        </label>
                    </div>
                    <div class="clearfix"></div>
                    <h3>Which area do you think need the most attention in Nigeria?</h3>

                    <div class="poll-goal-wrap">
                      @foreach($goals as $goal)
                        <input type="radio" class="radio_item" value="" name="item" >
                        <label class="label_item" for="radio1"> <img src="{{$goal->icon}}"> </label>
                      @endforeach
                    </div>
                    </form>
                </div>
                </div>
                <div class="modal-footer pb-5">
                <button type="button" class="btn btn-primary poll-btn">Save changes</button>
                <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
                </div>
            </div>
            </div>
        </div>
        <script src="{{ asset('public/js/popper.min.js') }}"></script>
        <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('public/js/argon.js') }}"></script>
        <script src="{{ asset('public/js/headroom.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="{{ asset('admin/js/sweetalert.min.js') }}"></script>
        <script>
            $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
            $('#sidebarCollapse2').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
            });
        </script>
        <!-- Include this after the sweet alert js file -->
        @include('sweet::alert')
    </body>
</html>

