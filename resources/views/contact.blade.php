@extends('layouts.app')
@section('content')
      
  <section id="contact-page">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          
        </div>
      </div>
    </div>
  </section>

  <section id="partners">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-4">
          <h2 class="">Our Partners</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
        <div class="col-md-8" align="right">
          <img src="{{asset('public/img/partners/1.png')}}">
          <img src="{{asset('public/img/partners/5.jpg')}}">
        </div>
      </div>
    </div>
  </section>
  
@endsection