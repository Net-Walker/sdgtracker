@extends('layouts.app_no_footer')
@section('content')
  <div class="wrapper">
    <!-- Sidebar -->
    <nav id="sidebar">
        <div class="row">
          <div class="col-lg-12">
          <div class="mobile-sidebar-close">    
              <img width="15" id="sidebarCollapse2" src="{{asset('public/img/close.png')}}">
            </div>
            <ul class="navbar-nav goal-menu mb-3">
              <li class="nav-item dropdown">
                <a class="nav-link" style="font-size:1rem!important; padding-top: 0;" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span><img class="select-icon" src="https://res.cloudinary.com/hy20xjlga/image/upload/v1547824869/favicon.png"></span> Select a Goal <i class="fa fa-chevron-down dropdown-icon"></i>
                </a>
                <div class="dropdown-menu" data-mcs-theme="minimal" aria-labelledby="navbarDropdown">
                @foreach($goals as $i => $goal)
                <a class="dropdown-item" href="{{route('indicators',['parameter'=>$goal->goal_id])}}"><img class="menu-icon" src="{{$goal->icon}}">Goal {{$i+1}}: {{str_limit($goal->title, $limit = 25, $end = '...')}}</a>
                  @endforeach
                </div>
              </li>
            </ul>
            <div class="side-goal-menu">
              <div>
                <img src="{{$goal_->icon}}">
              </div>
              <h2>Goal {{$goal_->goal_id}} {{$goal_->title}}</h2>
              <p>{{$targets->count()}} Targets, <span>{{$count_indicators}} Indicators</span></p>
            </div>
          </div>
        </div>
    </nav>
    <div id="content">
      <div class="mobile-sidebar-toggle">    
        <img width="20" id="sidebarCollapse" src="{{asset('public/img/menu-button.png')}}">
      </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="tracker-details">
              <div class="row">
                <div class="col-12">
                  <h2 class="mb-5">{{$goal_->long_title}}</h2>
                </div>
              </div>
              @foreach($indicators as $n => $target)
              <div class="row">
                <div class="col-lg-12">
                  <div class="tracker-card">
                    <h3 class="mb-5">{{$target->target_title}}</h3>
                    <div class="row">
                      <div class="col-lg-12">
                      @foreach($target->indicators as $i => $indicator)
                        <a href="{{route('indicators-details',['parameter'=>$goal_->goal_id])}}">
                        <div class="progress-wrap">
                          <h4>{{$goal_->goal_id}}.{{$n+1}}.{{$i+1}} {{str_limit($indicator->title, $limit = 65, $end = '...')}}</h4> 
                          <div class="progress-wrapper">
                            <div class="progress-info">
                              <div class="progress-label">
                              @if (empty($indicator->indicator_trackers->last()->year))
                              <span></span>
                              @else
                              <span>{{$indicator->indicator_trackers->last()->year}}</span>
                              @endif
                                
                              </div>
                              <div class="progress-percentage">
                              @if (empty($indicator->indicator_trackers->last()->progress))
                              <span></span>
                              @else
                              <span>{{$indicator->indicator_trackers->last()->progress}}%</span>
                              @endif
                                
                              </div>
                            </div>
                            <div class="progress">
                            @if (empty($indicator->indicator_trackers->last()->progress))
                              <span></span>
                              @else
                              <div class="progress-bar bg-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{$indicator->indicator_trackers->last()->progress}}%;"></div>
                              @endif
                              
                            </div>
                          </div>
                        </div>
                        </a>
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                @endforeach
            </div>
          </div>
        </div>
    </div>
  </div> 
  <style>
  </style>
  @endsection