
@extends('layouts.app')

@section('content')
  <section id="home">
    <div class="container-fluid">
      <div class="row align-items-center">
      <img class="nigeria-map" src="{{asset('public/img/naija.png')}}">
        <div id="left" class="col-lg-6 no-gutter">
          <div class="left-wrap">
          <img src="{{asset('public/img/ossap.png')}}">
          <h2 class="mt-3">SDG Tracker for Nigeria By The Office of the<br/> Special Assistant to the President on SDGs.</h2>
          <p>
            The Sustainable Development Goals (SDGs) are a <br/>
            universal call to action to end poverty, protect the planet <br/>
            and ensure that all people enjoy peace and prosperity.
          </p>          
          <p class="action hvr-icon-wobble-horizontal">Select a Goal to get started.</p>
          </div>
        </div>
        <div id="right" class="col-lg-6 no-gutter" align="left">
          <!-- <a href="goal.html"><img class="mt-5" src="img/goals.png"></a> -->
          <div class="goals">
            @foreach($goals as $goal)
            <div class="goal-wrap">
              <a class="hvr-pop" href="{{route('goal',['parameter'=>$goal->goal_id])}}">
                <img src="{{$goal->icon}}">
              </a>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="partners">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-4">
          <h2 class="">Our Partners</h2>
          <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua.</p> -->
        </div>
        <div class="col-md-8" align="right">
          <img src="{{asset('public/img/partners/1.png')}}">
          <!-- <img src="{{asset('public/img/partners/2.png')}}">
          <img src="{{asset('public/img/partners/3.png')}}">
          <img src="{{asset('public/img/partners/4.png')}}"> -->
          <img src="{{asset('public/img/partners/5.jpg')}}">
        </div>
        <!-- <div class="col-md-2" align="center">
          <img src="{{asset('public/img/partners/2.png')}}">
        </div>
        <div class="col-md-2" align="center">
          <img src="{{asset('public/img/partners/3.png')}}">
        </div>
        <div class="col-md-2" align="center">
          <img src="{{asset('public/img/partners/4.png')}}">
        </div> -->
      </div>
    </div>
  </section>
  @endsection


