@extends('layouts.app_no_footer')
@section('content')

    <!-- Page Content -->
    <div id="content">
        <div class="mobile-sidebar-toggle">    
          <img width="20" id="sidebarCollapse" src="img/menu-button.png">
        </div>
        <section id="p-opinion-page">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-12">
                <h2>Public Opinion Concerning the United Nations Sustainable Development Goals in Nigeria.</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.</p>
              </div>
              <div class="col-lg-12">
                <h3>Do you think that Nigeria will achieve its goals by 2030?</h3>
                <div id="piechart"></div>
              </div>
              <div class="col-lg-12">
                <h3>Which area do you think need the most attention in Nigeria?</h3>
                <div class="row mt-3">
                  <div class="col-lg-5">

                    <div class="progress-wrapper">
                      <div class="progress-info">
                        <div class="progress-label">
                          <span>Task completed</span>
                        </div>
                        <div class="progress-percentage">
                          <span>60%</span>
                        </div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-primary goal1" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                      </div>
                    </div>

                    <div class="progress-wrapper">
                      <div class="progress-info">
                        <div class="progress-label">
                          <span>Task completed</span>
                        </div>
                        <div class="progress-percentage">
                          <span>60%</span>
                        </div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-primary goal2" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                      </div>
                    </div>

                    <div class="progress-wrapper">
                      <div class="progress-info">
                        <div class="progress-label">
                          <span>Task completed</span>
                        </div>
                        <div class="progress-percentage">
                          <span>60%</span>
                        </div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-primary goal3" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                      </div>
                    </div>

                    <div class="progress-wrapper">
                      <div class="progress-info">
                        <div class="progress-label">
                          <span>Task completed</span>
                        </div>
                        <div class="progress-percentage">
                          <span>60%</span>
                        </div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-primary goal4" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                      </div>
                    </div>

                    <div class="progress-wrapper">
                      <div class="progress-info">
                        <div class="progress-label">
                          <span>Task completed</span>
                        </div>
                        <div class="progress-percentage">
                          <span>60%</span>
                        </div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-primary goal5" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                      </div>
                    </div>

                    <div class="progress-wrapper">
                      <div class="progress-info">
                        <div class="progress-label">
                          <span>Task completed</span>
                        </div>
                        <div class="progress-percentage">
                          <span>60%</span>
                        </div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-primary goal6" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                      </div>
                    </div>

                    <div class="progress-wrapper">
                      <div class="progress-info">
                        <div class="progress-label">
                          <span>Task completed</span>
                        </div>
                        <div class="progress-percentage">
                          <span>60%</span>
                        </div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-primary goal7" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                      </div>
                    </div>

                    <div class="progress-wrapper">
                      <div class="progress-info">
                        <div class="progress-label">
                          <span>Task completed</span>
                        </div>
                        <div class="progress-percentage">
                          <span>60%</span>
                        </div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-primary goal8" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                      </div>
                    </div>

                    <div class="progress-wrapper">
                      <div class="progress-info">
                        <div class="progress-label">
                          <span>Task completed</span>
                        </div>
                        <div class="progress-percentage">
                          <span>60%</span>
                        </div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-primary goal9" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                      </div>
                    </div>

                    <div class="progress-wrapper">
                      <div class="progress-info">
                        <div class="progress-label">
                          <span>Task completed</span>
                        </div>
                        <div class="progress-percentage">
                          <span>60%</span>
                        </div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-primary goal10" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                      </div>
                    </div>

                  </div>
                  <div class="col-lg-5 offset-1">

                    <div class="progress-wrapper">
                      <div class="progress-info">
                        <div class="progress-label">
                          <span>Task completed</span>
                        </div>
                        <div class="progress-percentage">
                          <span>60%</span>
                        </div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-primary goal11" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                      </div>
                    </div>

                    <div class="progress-wrapper">
                      <div class="progress-info">
                        <div class="progress-label">
                          <span>Task completed</span>
                        </div>
                        <div class="progress-percentage">
                          <span>60%</span>
                        </div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-primary goal12" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                      </div>
                    </div>

                    <div class="progress-wrapper">
                      <div class="progress-info">
                        <div class="progress-label">
                          <span>Task completed</span>
                        </div>
                        <div class="progress-percentage">
                          <span>60%</span>
                        </div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-primary goal13" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                      </div>
                    </div>

                    <div class="progress-wrapper">
                      <div class="progress-info">
                        <div class="progress-label">
                          <span>Task completed</span>
                        </div>
                        <div class="progress-percentage">
                          <span>60%</span>
                        </div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-primary goal14" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                      </div>
                    </div>

                    <div class="progress-wrapper">
                      <div class="progress-info">
                        <div class="progress-label">
                          <span>Task completed</span>
                        </div>
                        <div class="progress-percentage">
                          <span>60%</span>
                        </div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-primary goal15" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                      </div>
                    </div>

                    <div class="progress-wrapper">
                      <div class="progress-info">
                        <div class="progress-label">
                          <span>Task completed</span>
                        </div>
                        <div class="progress-percentage">
                          <span>60%</span>
                        </div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-primary goal16" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                      </div>
                    </div>

                    <div class="progress-wrapper">
                      <div class="progress-info">
                        <div class="progress-label">
                          <span>Task completed</span>
                        </div>
                        <div class="progress-percentage">
                          <span>60%</span>
                        </div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-primary goal17" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    </div>

@endsection

