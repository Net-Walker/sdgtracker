@extends('layouts.auth')
@section('content')
<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth" style="background: url(public/img/auth-back.jpg); background-size:cover;background-repeat:no-repeat;">
            <div class="row w-100">
                <div class="col-lg-5 mx-auto">
                    <div class="auth-form-light text-left p-5">
                        <div class="brand-logo">
                          <a href="{{ route('index') }}"><img src="{{asset('public/img/logo.png')}}"/></a>
                        </div>
                        <h4>Hello! there welcome back</h4>
                        <h6 class="font-weight-light">Sign in to continue.</h6>
                        <form class="pt-3" method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                            @csrf
                            <div class="form-group">
                              <input type="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Email"  name="email" value="{{ old('email') }}" required autofocus>
                            </div>
                            <div class="form-group">
                              <input type="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password" name="password" required>
                            </div>
                            <div class="mt-3">
                              <button type="submit" class="btn btn-primary" style="width:100%">{{ __('Login') }}</button>
                            </div>
                            <div class="my-2 d-flex justify-content-between align-items-center">
                                <div class="form-check">
                                    <label class="form-check-label text-muted">
                                      <input type="checkbox" class="form-check-input">
                                      Keep me signed in
                                    </label>
                                </div>
                                <a href="#" class="auth-link text-black">Forgot password?</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
@endsection


