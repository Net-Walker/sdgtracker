@extends('layouts.app')
@section('content')

@endsection

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>SDG Tracker For Nigeria</title>
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/hover.min.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,700,900" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Marck+Script" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
  <link rel="icon" type="image/png" href="img/fav.png" />
</head>
<body id="trackerpg">
  <nav class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light">
    <div class="container-fluid">
      <a class="navbar-brand" href="index.html"><img src="img/logo.png"></a>
      <button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#navbar-primary" aria-controls="navbar-primary" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbar-primary">
        <div class="navbar-collapse-header">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a href="index.html">
                <img src="img/logo.png">
              </a>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-primary" aria-controls="navbar-primary" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
          <li class="nav-item">
            <a class="nav-link" href="indicator.html">SDG Tracker</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="indicator-details.html">Indicators</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="modal" data-target="#modal-default">Public Opinion</a>
          </li>
        </ul>
        <ul class="navbar-nav align-items-lg-center ml-lg-auto">

          <li class="nav-item">
            <a class="nav-link" href="#">Admin Login</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- Modal -->
  <div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="modal-title-default"></h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <div id="p-opinion">
            <h2>What do you think?</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat.</p>
            <h3 class="mb-4">Do you think that Nigeria will achieve its goals by 2030?</h3>
            <form class="mb-3">
              <div class="custom-control custom-radio mb-3 mr-3">
                <input name="custom-radio-1" class="custom-control-input" id="customRadio1" type="radio">
                <label class="custom-control-label" for="customRadio1">
                  <span>Yes</span>
                </label>
              </div>
              <div class="custom-control custom-radio mb-3 mr-3">
                <input name="custom-radio-1" class="custom-control-input" id="customRadio2" type="radio">
                <label class="custom-control-label" for="customRadio2">
                  <span>No</span>
                </label>
              </div>
              <div class="custom-control custom-radio mb-3 mr-3">
                <input name="custom-radio-1" class="custom-control-input" id="customRadio3" type="radio">
                <label class="custom-control-label" for="customRadio3">
                  <span>Don't know</span>
                </label>
              </div>
              <div class="clearfix"></div>
              <h3>Which area do you think need the most attention in Nigeria?</h3>

              <div class="poll-goal-wrap">

                <input type="radio" class="radio_item" value="" name="item" id="radio1">
                <label class="label_item" for="radio1"> <img src="img/goal-icon/1.png"> </label>

                <input type="radio" class="radio_item" value="" name="item" id="radio2">
                <label class="label_item" for="radio2"> <img src="img/goal-icon/2.png"> </label>

                <input type="radio" class="radio_item" value="" name="item" id="radio3">
                <label class="label_item" for="radio3"> <img src="img/goal-icon/3.png"> </label>

                <input type="radio" class="radio_item" value="" name="item" id="radio4">
                <label class="label_item" for="radio4"> <img src="img/goal-icon/4.png"> </label>

                <input type="radio" class="radio_item" value="" name="item" id="radio5">
                <label class="label_item" for="radio5"> <img src="img/goal-icon/5.png"> </label>

                <input type="radio" class="radio_item" value="" name="item" id="radio6">
                <label class="label_item" for="radio6"> <img src="img/goal-icon/6.png"> </label>

                <input type="radio" class="radio_item" value="" name="item" id="radio7">
                <label class="label_item" for="radio7"> <img src="img/goal-icon/7.png"> </label>

                <input type="radio" class="radio_item" value="" name="item" id="radio8">
                <label class="label_item" for="radio8"> <img src="img/goal-icon/8.png"> </label>

                <input type="radio" class="radio_item" value="" name="item" id="radio9">
                <label class="label_item" for="radio9"> <img src="img/goal-icon/9.png"> </label>

                <input type="radio" class="radio_item" value="" name="item" id="radio10">
                <label class="label_item" for="radio10"> <img src="img/goal-icon/10.png"> </label>

                <input type="radio" class="radio_item" value="" name="item" id="radio11">
                <label class="label_item" for="radio11"> <img src="img/goal-icon/11.png"> </label>

                <input type="radio" class="radio_item" value="" name="item" id="radio12">
                <label class="label_item" for="radio12"> <img src="img/goal-icon/12.png"> </label>

                <input type="radio" class="radio_item" value="" name="item" id="radio13">
                <label class="label_item" for="radio13"> <img src="img/goal-icon/13.png"> </label>

                <input type="radio" class="radio_item" value="" name="item" id="radio14">
                <label class="label_item" for="radio14"> <img src="img/goal-icon/14.png"> </label>

                <input type="radio" class="radio_item" value="" name="item" id="radio15">
                <label class="label_item" for="radio15"> <img src="img/goal-icon/15.png"> </label>

                <input type="radio" class="radio_item" value="" name="item" id="radio16">
                <label class="label_item" for="radio16"> <img src="img/goal-icon/16.png"> </label>

                <input type="radio" class="radio_item" value="" name="item" id="radio17">
                <label class="label_item" for="radio17"> <img src="img/goal-icon/17.png"> </label>

              </div>
            </form>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary">Save changes</button>
          <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal End -->

  <div class="wrapper">

    <!-- Sidebar -->
    <nav id="sidebar">
        <div class="row">
          <div class="col-lg-12">
            <div class="mobile-sidebar-close">    
              <img width="15" id="sidebarCollapse2" src="img/close.png">
            </div>
            <ul class="navbar-nav goal-menu mb-3">
              <li class="nav-item dropdown">
                <a class="nav-link" style="font-size:1rem!important; padding-top: 0;" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span><img class="select-icon" src="img/fav.png"></span> Select a Goal <i class="fa fa-chevron-down dropdown-icon"></i>
                </a>
                <div class="dropdown-menu mCustomScrollbar" data-mcs-theme="minimal" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="goal1.html"><img class="menu-icon" src="img/goal-icon/1.png">Goal 1: No Poverty</a>
                  <a class="dropdown-item" href="goal2.html"><img class="menu-icon" src="img/goal-icon/2.png">Goal 2: Zero Hunger</a>
                  <a class="dropdown-item" href="goal3.html"><img class="menu-icon" src="img/goal-icon/3.png">Goal 3: Good Health</a>
                  <a class="dropdown-item" href="goal4.html"><img class="menu-icon" src="img/goal-icon/4.png">Goal 4: Quality Education</a>
                  <a class="dropdown-item" href="goal5.html"><img class="menu-icon" src="img/goal-icon/5.png">Goal 5: Gender Equality</a>
                  <a class="dropdown-item" href="goal6.html"><img class="menu-icon" src="img/goal-icon/6.png">Goal 6: Clean Water and...</a>
                  <a class="dropdown-item" href="goal7.html"><img class="menu-icon" src="img/goal-icon/7.png">Goal 7: Affordable and...</a>
                  <a class="dropdown-item" href="goal8.html"><img class="menu-icon" src="img/goal-icon/8.png">Goal 8: Decent Work and...</a>
                  <a class="dropdown-item" href="goal9.html"><img class="menu-icon" src="img/goal-icon/9.png">Goal 9: Infrastructure...</a>
                  <a class="dropdown-item" href="goal10.html"><img class="menu-icon" src="img/goal-icon/10.png">Goal 10: Reduced Inequalities</a>
                  <a class="dropdown-item" href="goal11.html"><img class="menu-icon" src="img/goal-icon/11.png">Goal 11: Sustainable Cities</a>
                  <a class="dropdown-item" href="goal12.html"><img class="menu-icon" src="img/goal-icon/12.png">Goal 12: Responsible...</a>
                  <a class="dropdown-item" href="goal13.html"><img class="menu-icon" src="img/goal-icon/13.png">Goal 13: Climate Action</a>
                  <a class="dropdown-item" href="goal14.html"><img class="menu-icon" src="img/goal-icon/14.png">Goal 14: Life Below Water</a>
                  <a class="dropdown-item" href="goal15.html"><img class="menu-icon" src="img/goal-icon/15.png">Goal 15: Life on Land</a>
                  <a class="dropdown-item" href="goal16.html"><img class="menu-icon" src="img/goal-icon/16.png">Goal 16: Peace, Justice...</a>
                  <a class="dropdown-item" href="goal17.html"><img class="menu-icon" src="img/goal-icon/17.png">Goal 17: Partnerships</a>
                </div>
              </li>
            </ul>
            <div class="side-goal-menu">
              <div>
                <img src="img/goal-icon/1.png">
              </div>
              <h2>Goal 1 End Poverty</h2>
              <p>10 Targets, <span>42 Indicators</span></p>
            </div>
          </div>
        </div>
    </nav>
    <!-- Page Content -->
    <div id="content">
        <div class="mobile-sidebar-toggle">    
          <img width="20" id="sidebarCollapse" src="img/menu-button.png">
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="tracker-details">
              <div class="row">
                <div class="col-6">
                  <h2 class="mb-5">1. End poverty in all its forms everywhere.</h2>
                </div>
                <div class="col-6" align="right">
                  <span><a href="indicator.html" class="btn btn-light btn-sm view-btn" tabindex="-1" role="button" aria-disabled="true"><img class="bck-icon" src="{{asset('img/chevron-left.svg')}}">Go Back</a></span>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="tracker-card">
                    <div class="intervention-list">
                      <div class="media mb-5">
                        <img src="..." class="mr-3" alt="...">
                        <div class="media-body">
                          <h5 class="mt-0">Name of Government Initiative/Intervention</h5>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                          proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                      </div>
                      <div class="media mb-5">
                        <img src="..." class="mr-3" alt="...">
                        <div class="media-body">
                          <h5 class="mt-0">Name of Government Initiative/Intervention</h5>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                          proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                      </div>
                      <div class="media mb-5">
                        <img src="..." class="mr-3" alt="...">
                        <div class="media-body">
                          <h5 class="mt-0">Name of Government Initiative/Intervention</h5>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                          proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
    </div>

  </div> 


  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/argon.js"></script>
  <script src="js/headroom.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
  <script>
    $(document).ready(function () {

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });

        $('#sidebarCollapse2').on('click', function () {
            $('#sidebar').toggleClass('active');
        });

    });

  </script>

</body>
</html>
