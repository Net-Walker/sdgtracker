
@extends('layouts.app')
@section('content')

  <section id="goal">
    <div class="container-fluid">
      <div class="row">
        <div id="left" class="col-lg-6 no-gutter">
          <div class="left-wrap">
            <div class="gpg-goal-wrap">
              <img width="100" class="mb-2" src="{{$goal_->icon}}">
              <ul class="navbar-nav goal-menu mb-3">
                <li class="nav-item dropdown">
                  <a class="nav-link" style="font-size:1rem!important; padding-top: 0; color:#FFFFFF;" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span><img class="select-icon" src="https://res.cloudinary.com/hy20xjlga/image/upload/v1547824869/favicon.png"></span> Select a Goal <i class="fa fa-chevron-down dropdown-icon"></i>
                  </a>
                  <div class="dropdown-menu" data-mcs-theme="minimal" aria-labelledby="navbarDropdown">
                    @foreach($goals as $i => $goal)
                      <a class="dropdown-item" href="{{route('goal',['parameter'=>$goal->goal_id])}}"><img class="menu-icon" src="{{$goal->icon}}">Goal {{$i+1}}: {{str_limit($goal->title, $limit = 25, $end = '...')}}</a>
                    @endforeach
                </li>
              </ul>
            </div>
            <h2>{{$goal_->long_title}}</h2>
            <p class="pt-2">
              {!! $goal_->description !!}
            </p>
          </div>
        </div>
        <div id="right" class="col-lg-6 pr-5">
          <div class="right-wrap">
            <h2>Goal {{$goal_->goal_id}}: {{$goal_->title}}</h2>
            <div class="progress-wrapper">
              <div class="progress-info">
                <div class="progress-label">
                  <span>Overall Progress</span>
                </div>
                <div class="progress-percentage">
                  <span>{{$progress}}%</span>
                </div>
              </div>
              <div class="progress">
                <div class="progress-bar bg-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{$progress}}%;"></div>
              </div>
            </div>
            <div class="tabs-optn">
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="target-tab" data-toggle="tab" href="#target" role="tab" aria-controls="target" aria-selected="true">Targets</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="link-tab" data-toggle="tab" href="#link" role="tab" aria-controls="link" aria-selected="false">Interventions</a>
                </li>
                <li class="nav-item ml-lg-auto">
                  <a href="{{route('indicators',['parameter'=>$goal_->goal_id])}}" class="btn btn-light btn-sm view-btn" tabindex="-1" role="button" aria-disabled="true"><span><img src="https://res.cloudinary.com/hy20xjlga/image/upload/v1547824869/favicon.png"></span> View Indicators</a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="target" role="tabpanel" aria-labelledby="target-tab">
                @foreach($targets as $i => $target)
                  <p>
                    {{$target->target_title}}
                  </p>
                  @endforeach
                </div>
                <div class="tab-pane fade" id="link" role="tabpanel" aria-labelledby="link-tab">
                  <div class="intervention">
                    @foreach($interventions as $intervention)
                    <div class="media mb-5">
                        <!-- <img width="100" src="{{$intervention->interventions_icon}}" class="mr-3" alt="..."> -->
                        <div class="media-body">
                          <h5 class="mt-0">{{$intervention->intervention_title}}</h5>
                            {{$intervention->interventions_description}}
                        </div>
                      </div>
                      @endforeach

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  @endsection