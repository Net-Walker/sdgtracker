<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@index')->name('index');
Route::get('/goal/{id}', 'PublicController@goal')->name('goal');
Route::get('/targets', 'PublicController@targets')->name('targets');
Route::get('/indicators/{id}', 'PublicController@indicators')->name('indicators');
Route::get('/indicators-details/{id}', 'PublicController@indicators_details')->name('indicators-details');
Route::get('/interventions', 'PublicController@interventions')->name('interventions');
Route::get('/trackers', 'PublicController@trackers')->name('trackers');

Route::get('/about', 'PublicController@about')->name('about');
Route::get('/contact-us', 'PublicController@contact')->name('contact-us');
Route::get('/privacy-policy', 'PublicController@privacy_policy')->name('privacy-policy');
Route::get('/public-opinions', 'PublicController@public_opinions')->name('public-opinions');
Route::get('/404', 'PublicController@notfound')->name('404');

Auth::routes();

Route::get('/home', 'HomeController@home')->name('home');
Route::get('/goals_', 'HomeController@goals_')->name('goals_');
Route::get('goal_/{id}','HomeController@goal_')->name('goal_');
Route::get('/target_/{id}','HomeController@target_')->name('target_');
Route::get('/targets_/{id}','HomeController@targets_')->name('targets_');
Route::get('/indicator_/{id}', 'HomeController@indicator_')->name('indicator_');
Route::get('/indicators_/{id}', 'HomeController@indicators_')->name('indicators_');
Route::get('/intervention_/{id}', 'HomeController@intervention_')->name('intervention_');
Route::get('/interventions_/{id}', 'HomeController@interventions_')->name('interventions_');
Route::get('/admin/interventions', 'HomeController@admin_interventions')->name('admin_interventions');
Route::get('/admin/indicators', 'HomeController@admin_indicators')->name('admin_indicators');
Route::get('/admin/manage_tracker/{id}', 'HomeController@manage_tracker')->name('manage_tracker');

Route::get('/public-opinion', 'HomeController@public_opinion')->name('public-opinion');
Route::post('record_public_opinions', 'PublicController@record_public_opinions')->name('record_public_opinions');

Route::get('/users', 'HomeController@users')->name('users');
Route::post('create_user', 'HomeController@create_user')->name('create_user');

Route::post('create_goal', 'HomeController@create_goal')->name('create_goal');
Route::post('edit_goal', 'HomeController@edit_goal')->name('edit_goal');
Route::get('delete_goal/{id}', 'HomeController@delete_goal')->name('delete_goal');

Route::post('create_target', 'HomeController@create_target')->name('create_target');
Route::post('edit_target', 'HomeController@edit_target')->name('edit_target');
Route::get('delete_target/{id}', 'HomeController@delete_target')->name('delete_target');

Route::post('create_indicator', 'HomeController@create_indicator')->name('create_indicator');
Route::get('delete_indicator/{id}', 'HomeController@delete_indicator')->name('delete_indicator');
Route::get('delete_indicator_progress/{id}', 'HomeController@delete_indicator_progress')->name('delete_indicator_progress');
Route::post('edit_indicator', 'HomeController@edit_indicator')->name('edit_indicator');
Route::post('update_progress', 'HomeController@update_progress')->name('update_progress');
Route::post('record_progress','HomeController@record_progress')->name('record_progress');

Route::post('create_intervention', 'HomeController@create_intervention')->name('create_intervention');
Route::get('delete_intervention/{id}', 'HomeController@delete_intervention')->name('delete_intervention');
Route::post('edit_intervention', 'HomeController@edit_intervention')->name('edit_intervention');