<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Goals;

class Interventions extends Model
{
    protected $fillable = [
        'intervention_title','goal_id',
    ];

    public function goal()
    {
        return $this->belongsTo(Goals::class);
    }
}
