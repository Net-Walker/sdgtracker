<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Indicators;
use App\NigeriaStates;

class IndicatorTracker extends Model
{
    public function states()
    {
        return $this->belongsTo(NigeriaStates::class,'nigeria_states_id');
    }
    
    public function indicators()
    {
        return $this->belongsTo(Indicators::class,'indicators_id');
    }

    protected $fillable = [
        'unit', 'progress','nigeria_states_id','indicators_id','year','state_code'
    ];
}
