<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Goals;
use App\Target;
use App\Interventions;
use App\Indicators;
use App\NigeriaStates;
use App\IndicatorTracker;
use App\User;
use App\Feedbacks;
use Alert;
use DB;

class HomeController extends Controller
{

        public function __construct()
        {
            $this->middleware('auth');
        }
        //Home
        public function home()
        {
            $goals = Goals::all();
            return view('admin.home',compact('goals'));  
        }
        //Home Ends

        //Goals
        public function goals_(){
            $goals = Goals::with('targets','interventions')->get();
            return view('admin.goals',compact('goals'));
        }
        public function goal_($id){
            $goal = Goals::findOrFail($id);
            $targets = Target::with('indicators')->where('goal_id',$goal->id)->get();
            return view('admin.goal',compact('goal','targets'));
        }
        public function create_goal(Request $request){
        }
        public function delete_goal($id){
            // $goal = Goals::find($id);    
            // $goal->delete();  
            // return redirect()->back();
        }
        public function edit_goal(Request $request){
            $goal = Goals::find($request->get('goal_id'));
            $goal->title = $request->get('title');
            $goal->long_title = $request->get('long_title');
            $goal->description = $request->get('description');
            $goal = $goal->save();
            if(!$goal){
                toastr()->error('An error has occurred please try again later.');
                return redirect()->back();
            }else{
                toastr()->success('Data has been edited successfully!');
                return redirect()->back();
            }
        }
        //Goals Ends

        //Targets
        public function targets_($id){
            $goals = Goals::all();
            $goal_ = Goals::where('goal_id',$id)->first();
            // dd($goal_);
            $targets = Target::with('indicators')->where('goal_id',$goal_->id)->get();
            return view('admin.targets',compact('goals','goal_','targets')); 
        }
        public function target_($id){
            $target = Target::where('id',$id)->first();
            $indicators = Indicators::with('target')->where('target_id', $target->id)->get();
            return view('admin.target', compact('target','indicators'));
        }
        public function create_target(Request $request){
            $target = new Target([
                'goal_id' => $request->get('goal_id'),
                'target_title'=> $request->get('target'),         
            ]);
            $target = $target->save();
            if(!$target){
                toastr()->error('An error has occurred please try again later.');
                return redirect()->back();
            }else{
                toastr()->success('Data has been saved successfully!');
                return redirect()->back();
            }
        }
        public function delete_target($id){
            $target = Target::find($id);    
            $target = $target->delete();  

            if(!$target){
                toastr()->error('An error has occurred please try again later.');
                return redirect()->back();
            }else{
                toastr()->success('Data has been deleted successfully!');
                return redirect()->back();
            }
        }
        public function edit_target(Request $request){  
            $target = Target::find($request->get('target_id'));
            $target->target_title = $request->get('target');
            $target = $target->save();
            if(!$target){
                toastr()->error('An error has occurred please try again later.');
                return redirect()->back();
            }else{
                toastr()->success('Data has been edited successfully!');
                return redirect()->back();
            }
        }
        //Targets Ends

        //Indicators
        public function indicators_($id)
        { 
            $indicators = Indicators::with('target')->where('target_id', $id)->get();
            $target_id = Indicators::where('target_id', $id)->first();
            $target = Target::where('id',$target_id->target_id)->first();
            $indicators_id = Indicators::where('target_id', $id)->get()->pluck('id')->toArray();
            $trackers = IndicatorTracker::with('states','indicators')->whereIn('indicators_id', $indicators_id)->get();
            return view('admin.indicators', compact('indicators', 'target', 'trackers'));
        }
        public function indicator_($id){
            $indicators = Indicators::where('id',$id)->get();
            $target = Target::where('id',$indicators[0]->target_id)->first();
            return view('admin.target', compact('indicators','target'));
        }
        public function create_indicator(Request $request){
            $target = new Indicators([
                'title'=> $request->get('indicator'),
                'progress' => 0,
                'graphical_rep' =>0,
                'target_id' => $request->get('target_id'),         
            ]);
           $target = $target->save();
            if(!$target){
                toastr()->error('An error has occurred please try again later.');
                return redirect()->back();
            }else{
                toastr()->success('Data has been saved successfully!');
                return redirect()->back();
            }
        }
        public function admin_indicators(){
            $indicators = Indicators::with('target')->get();

        // $indicators = Indicators::with('target')->paginate(20);
            return view('admin.all_indicators',compact('indicators'));
        }
        public function edit_indicator(Request $request){
            $indicator = Indicators::find($request->get('indicator_id'));
            $indicator->title = $request->get('indicator');
            $indicator= $indicator->save();
            if(!$indicator){
                toastr()->error('An error has occurred please try again later.');
                return redirect()->back();
            }else{
                toastr()->success('Data has been edited successfully!');
                return redirect()->back();
            }
        }
        public function delete_indicator($id){
            $indicator = Indicators::find($id);    
            $indicator = $indicator->delete();  
            if(!$indicator){
                toastr()->error('An error has occurred please try again later.');
                return redirect()->back();
            }else{
                toastr()->success('Data has been deleted successfully!');
                return redirect()->back();
            }
        }

        public function delete_indicator_progress($id){
            $indicator = IndicatorTracker::find($id);    
            $indicator = $indicator->delete();  
            if(!$indicator){
                toastr()->error('An error has occurred please try again later.');
                return redirect()->back();
            }else{
                toastr()->success('Data has been deleted successfully!');
                return redirect()->back();
            }
        }

        
        //Indicators Ends

        //Interventions
        public function interventions_($id){
            $goals = Goals::all();
            $goal_ = Goals::where('goal_id',$id)->first();
            $interventions = Interventions::where('goal_id',$goal_->id)->get();
            return view('admin.interventions', compact('interventions','goal_'));
        }

        public function intervention_($id){
            $intervention = Interventions::where('id',$id)->get();
            return view('admin.intervention', compact('intervention'));
        }

        public function create_intervention(Request $request){
            $intervention = new Interventions([
                'goal_id' => $request->get('goal_id'),
                'intervention_title'=> $request->get('intervention'),         
            ]);
            $intervention = $intervention->save();
            if(!$intervention){
                toastr()->error('An error has occurred please try again later.');
                return redirect()->back();
            }else{
                toastr()->success('Data has been saved successfully!');
                return redirect()->back();
            }
        }

        public function edit_intervention(Request $request){
            $intervention = Interventions::find($request->get('intervention_id'));
            $intervention->intervention_title = $request->get('intervention');
            $intervention = $intervention->save();
            if(!$intervention){
                toastr()->error('An error has occurred please try again later.');
                return redirect()->back();
            }else{
                toastr()->success('Data has been edited successfully!');
                return redirect()->back();
            }
        }

        public function delete_intervention($id){
            $intervention = Interventions::find($id);    
            $intervention = $intervention->delete();  
            if(!$intervention){
                toastr()->error('An error has occurred please try again later.');
                return redirect()->back();
            }else{
                toastr()->success('Data has been edited successfully!');
                return redirect()->back();
            }
        }

        public function manage_tracker($id){
            $indicator = Indicators::findOrFail($id);
            $trackers = IndicatorTracker::with('states','indicators')->where('indicators_id', $id)->get();
            $states = NigeriaStates::all();
            $trackers_graphs = IndicatorTracker::groupBy('year')->selectRaw('sum(progress) div count(progress) as progress, year')->where('indicators_id', $id)->get('progress','year');    
            return view('admin.trackers', compact('trackers','indicator','states','trackers_graphs'));
        }

        public function update_progress(Request $request){
            $indicator_id =  IndicatorTracker::find($request->get('tracker_id'))->indicators_id;
            $update_progress = IndicatorTracker::find($request->get('tracker_id'));
            $update_progress->progress = $request->get('growth');
            $update_progress->year = $request->get('year');
            $update_progress = $update_progress->save();
            if(!$update_progress){
                toastr()->error('An error has occurred please try again later.');
                return redirect()->back();
            }else{
                $update_parent_indicator_progress = IndicatorTracker::where('indicators_id',$indicator_id)->sum('progress');
                $update_parent_indicator_progress_count = IndicatorTracker::where('indicators_id',$indicator_id)->count();
                $indicator_progress = $update_parent_indicator_progress/$update_parent_indicator_progress_count;
                $update_indicator_progress = Indicators::find($indicator_id);
                $update_indicator_progress->progress = $indicator_progress;
                $update_indicator_progress = $update_indicator_progress->save();
                toastr()->success('Data has been updated successfully!');
                return redirect()->back();
            }
        }

        public function record_progress(Request $request){
            $growth = new IndicatorTracker([
                'unit' => '%',
                'progress' => $request->get('growth'),
                'nigeria_states_id'=> $request->get('ng_state'),
                'indicators_id'=> $request->get('indicator_id'),
                'year'=>  $request->get('year'),            
            ]);
            $growth = $growth->save();
            if(!$growth){
                toastr()->error('An error has occurred please try again later.');
                return redirect()->back();
            }else{
                $indicator_id = $request->get('indicator_id');
                $update_parent_indicator_progress = IndicatorTracker::where('indicators_id',$indicator_id)->sum('progress');
                $update_parent_indicator_progress_count = IndicatorTracker::where('indicators_id',$indicator_id)->count();
                $indicator_progress = $request->get('growth');
                $update_indicator_progress = Indicators::find($indicator_id);
                $update_indicator_progress->progress = $indicator_progress;
                $update_indicator_progress = $update_indicator_progress->save();
                toastr()->success('Data has been recorded successfully!');
                return redirect()->back();
            }
        }

        public function create_interventions(Request $request){
            
        }
        //Interventions Ends

        //Public Opinions
        public function public_opinion()
        {   $opinions = Feedbacks::all();
            return view('admin.public_opinions',compact('opinions'));
        }
        //Public Opinions Ends

        //Users
        public function users()
        {   
            $users = User::all();
            return view('admin.users', compact('users'));
        }
        public function create_user(Request $request){
            $user = new User([
                'name' => $request->get('name'),
                'email'=> $request->get('email'),
                'role'=> $request->get('role'),
                'password'=>  Hash::make($request->get('password')),            
            ]);
            $user = $user->save();
            if(!$user){
                toastr()->error('An error has occurred please try again later.');
                return redirect()->back();
            }else{
                toastr()->success('Data has been recorded successfully!');
                return redirect()->back();
            }
        }
        //Users ENds
        public function admin_interventions(){
            $interventions = Interventions::get();
            return view('admin.all_interventions', compact('interventions'));
        }
}
