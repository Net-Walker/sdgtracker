<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Goals;
use App\Target;
use App\Interventions;
use App\Indicators;
use App\Feedbacks;
use App\NigeriaStates;
use App\IndicatorTracker;
use Alert;
use DB;

class publicController extends Controller
{
    public function index(){
        $goals = Goals::all();
        return view('index',compact('goals'));  
    }

    public function goal($id){
        $goals = Goals::all();
        $goal_ = Goals::where('goal_id',$id)->first();
        $targets = Target::where('goal_id',$goal_->id)->get();
        $targets_percent = Target::where('goal_id',$goal_->id)->pluck('goal_id')->toArray();
        $goal_progress = Target::with('indicators')->whereIn('goal_id', $targets_percent)->get();
        $indicators_progress_count = $goal_progress->pluck('indicators')->collapse()->count();
        $indicators_progress = $goal_progress->pluck('indicators')->collapse()->sum('progress');
        $progress = number_format($indicators_progress/$indicators_progress_count);      
        $interventions = Interventions::where('goal_id',$goal_->id)->get();
        return view('goal',compact('goals','goal_','targets','interventions','goal_progress','progress'));  
    
    }
    public function targets(){
        return view('index');  
    
    }
    public function indicators($id_){
        $goals = Goals::all();
        $id = Goals::where('goal_id',$id_)->first()->id;
        $target = Target::where('goal_id',$id)->first();
        if($target === null){
            return view('404');   
        }
        $goal_ = Goals::find($target->goal_id);
        $targets = Target::where('goal_id',$id)->get();
        $targets_id= Target::where('goal_id',$id)->pluck('id')->toArray();
        $interventions = Interventions::where('goal_id',$id)->get();
        $indicators = Target::with('indicators','indicators.indicator_trackers')->where('goal_id', $target->goal_id)->get();
        $count_indicators = Indicators::orWhereIn('target_id',$targets_id)->count();
        return view('indicator',compact('goals','goal_','targets','indicators','count_indicators'));  
    }

    public function indicators_details($id_){
        $goals = Goals::all();
        $id = Goals::where('goal_id',$id_)->first()->id;
        $target = Target::where('goal_id',$id)->first();
        if($target === null){
            return view('404');   
        }
        $goal_ = Goals::find($target->goal_id);
        $targets = Target::where('goal_id',$id)->get();
        $targets_id= Target::where('goal_id',$id)->pluck('id')->toArray();
        $interventions = Interventions::where('goal_id',$id)->get();
        $indicators_id= Target::with('indicators')->where('goal_id', $target->goal_id)->pluck('id')->toArray();
        $indicators = Target::with(['indicators','indicators.indicator_trackers' => function($query){
            $query->select('year', \DB::raw('sum(progress) div count(progress) as progress'), 'indicators_id','nigeria_states_id','state_code')->groupBy('indicators_id','year');
        }])->where('goal_id', $target->goal_id)->get();

        // dd($indicators);

        $count_indicators = Indicators::orWhereIn('target_id',$targets_id)->count();
        return view('indicator-details',compact('goals','goal_','targets','indicators','count_indicators')); 
    }
    
    public function notfound(){
        return view('404'); 
    }
    public function interventions(){
        $goals = Goals::all();
        return view('interventions',compact('goals'));  
    }

    public function trackers(){
        $goals = Goals::all();
        return view('tracker',compact('goals'));
    }
    
    public function about(){
        $goals = Goals::all();
        return view('about',compact('goals'));  
    }

    public function contact(){
        $goals = Goals::all();
        return view('contact',compact('goals'));  
    }

    public function privacy_policy(){
        $goals = Goals::all();
        return view('privacy-policy',compact('goals'));  
    }

    public function public_opinions(){
        return view('public-opinion');  
    }
    public function record_public_opinions(Request $request){
        $opinions = new Feedbacks([
            'feedback'=> $request->get('feedback'),
            'achieve_goals'=> $request->get('achieve_goals'),
            'goals_that_needs_attention'=>  $request->get('goals_that_needs_attention'),            
        ]);
        $opinions = $opinions->save();
        if(!$opinions){
            toastr()->error('An error has occurred please try again later.');
            return redirect()->back();
        }else{
            toastr()->success('Data has been recorded successfully!');
            return redirect()->back();
        }
    }

}
