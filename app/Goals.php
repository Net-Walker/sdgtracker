<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Targets;
use App\Indicators;
use App\Feedbacks;
use App\Interventions;

class Goals extends Model
{
   protected $fillable = [
      'goal_id', 'title', 'long_title', 'icon', 'image', 'background_color', 'description'
   ];

   public function targets(){
      return $this->hasMany(Target::class, 'goal_id');
   }

   public function indicators(){
      return $this->hasMany(Indicators::class);
   }

   public function feedbacks(){
      return $this->hasMany(Feedbacks::class);
   }

   public function interventions(){
      return $this->hasMany(Interventions::class, 'goal_id');
   }
}
