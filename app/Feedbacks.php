<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Goals;

class Feedbacks extends Model
{
    public function goals()
    {
        return $this->belongsTo(Goals::class);
    }
    protected $fillable = [
        'feedback', 'achieve_goals', 'goals_that_needs_attention'
    ];
}
