<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Goals;
use App\Indicators;

class Target extends Model
{
    public function goal()
    {
        return $this->belongsTo(Goals::class);
    }

    public function indicators()
    {
        return $this->hasMany('App\Indicators','target_id','id');
    }
    
    protected $fillable = [
        'goal_id', 'target_title'
    ];
}
