<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Target;
use App\IndicatorTracker;
use App\NigeriaStates;

class Indicators extends Model
{
    protected $fillable = [
        'title','progress','graphical_rep', 'target_id'
    ];

    public function target()
    {
        return $this->belongsTo('App\Target');
    }
    public function indicator_trackers()
    {
        return $this->hasMany('App\IndicatorTracker','indicators_id','id');
        
    }
    public function states()
    {
        return $this->belongsTo('App\NigeriaStates','nigeria_states_id','id');
    }
}
