<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndicatorTrackersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indicator_trackers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unit');
            $table->string('progress');
            
            $table->integer('nigeria_states_id')->unsigned()->index()->nullable();
            $table->foreign('nigeria_states_id')->references('id')->on('nigeria_states');

            $table->integer('indicators_id')->unsigned()->index()->nullable();
            $table->foreign('indicators_id')->references('id')->on('indicators');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indicator_trackers');
    }
}
